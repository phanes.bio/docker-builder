FROM docker/buildx-bin:v0.11 as buildx

FROM docker:24

ARG GIT_VERSION=2.40.1-r0
ARG CURL_VERSION=8.2.1-r0
ARG JQ_VERSION=1.6-r3

COPY --from=buildx /buildx /usr/libexec/docker/cli-plugins/docker-buildx

RUN apk --no-cache --update-cache add git="$GIT_VERSION" curl="$CURL_VERSION" jq="$JQ_VERSION" && \
    mkdir -p /etc/docker && \
    echo "{ \"insecure-registries\": [\"127.0.0.1:5000\"] }" >> /etc/docker/daemon.json && \
    addgroup docker && \
    adduser -g "" -s /sbin/nologin -G docker -S -D builderuser

USER builderuser

RUN mkdir ~/.docker && \
    curl -fsSL https://raw.githubusercontent.com/docker/scout-cli/main/install.sh -o ~/install-scout.sh && \
    DOCKER_HOME=~/.docker sh ~/install-scout.sh

HEALTHCHECK NONE