# Docker Builder

This is a repository containing a GitLab CI file which automatically builds and pushes Docker images.

## Configuration

You may change the name (`name`) of the image using the `IMAGE_NAME` environment variable of the job which defaults to the projects slug.

Tagged versions are always `latest` and the short commit SHA of the commit that triggered the build.

It is adviced not to change the `IMAGE_PATH` as the used token may not have access to the registry/project you want to push to.

The pipeline automatically builds your image for `linux/amd64` as well as `linux/arm64`. If you wish to change this you may override the `BUILDX_PLATFORMS` variable.

## Usage

Simple! Include it in your GitLab CI file and if there's a Dockerfile and a push to `main` it'll automatically build and push the image:

```yaml
include: "https://gitlab.com/phanes.bio/docker-builder/-/raw/main/build-and-publish-image.yml"
```
